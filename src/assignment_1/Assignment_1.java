/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package assignment_1;

/**
 *
 * @author Mostafa Rafea
 */
import java.util.*; 

public class Assignment_1 {

    /**
     * @param args the command line arguments
     */
    
    public static void main(String[] args) {
        
        Queue q = new Queue();
        
        q.enqueue(1);
        q.enqueue(2);
        q.enqueue(3);
        q.enqueue(4);
        q.display();
        
        q.dequeue();
        q.display();
        
        q.enqueue(5);
        q.display();
        
        q.dequeue();
        q.dequeue();
        q.display();
        
    }
    
}

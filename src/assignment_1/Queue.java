/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package assignment_1;

/**
 *
 * @author Mostafa Rafea
 */
 
import java.util.*; 

public class Queue {
    
    private Stack<Integer> stack1 = new Stack<Integer>();
    private Stack<Integer> stack2 = new Stack<Integer>();
    
    public void enqueue (int element) {
        stack1.push(element);
    }
    
    public int dequeue () {
        while (!stack1.isEmpty()){
            stack2.push(stack1.pop());
        }
        
        int deletedElement = stack2.pop();
        
        while (!stack2.isEmpty()) {
            stack1.push(stack2.pop());
        }
        
        return deletedElement;
    } 
    
    public void display () {
        System.out.println(stack1);
    }
}
